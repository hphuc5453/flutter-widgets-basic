import 'dart:async';

import 'package:demo_random_number/blocs/temp_bloc.dart';
import 'package:demo_random_number/models/elapsed_time.dart';
import 'package:demo_random_number/ui/timer_state.dart';
import 'package:flutter/cupertino.dart';

class TimerTextWidget extends StatefulWidget {
  TimerTextWidget(this.tempBloc);
  TempBloc tempBloc;

  @override
  State<StatefulWidget> createState() => _TimerTextState(tempBloc);
}

class _TimerTextState extends State<TimerTextWidget> {
  _TimerTextState(this.tempBloc);
  TempBloc tempBloc;
  late Timer timer;
  int milliseconds = 0;

  @override
  void initState() {
    timer = Timer.periodic(Duration(milliseconds: tempBloc.timerMillisecondsRefreshRate), callback);
    super.initState();
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  void callback(Timer timer){
    if(milliseconds != tempBloc.stopwatch.elapsedMilliseconds){
      milliseconds = tempBloc.stopwatch.elapsedMilliseconds;
      final int seconds = ((milliseconds / 10).truncate() / 100).truncate();
      final int minutes = (seconds / 60).truncate();
      final ElapsedTime elapsedTime = ElapsedTime(minutes, seconds);
      for(final listener in tempBloc.timerListeners){
        listener(elapsedTime);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return TimerWidget(tempBloc);
  }
}
