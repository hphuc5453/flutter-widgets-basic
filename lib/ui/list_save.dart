import 'package:demo_random_number/models/temperature.dart';
import 'package:flutter/material.dart';

class ListSaveWidget extends StatefulWidget{
  ListSaveWidget(this.listData);
  List<Temperature> listData;

  @override
  State<StatefulWidget> createState() => _ListSaveState(listData);

}

class _ListSaveState extends State<ListSaveWidget>{
  _ListSaveState(this.listData);
  List<Temperature> listData;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: listData.length,
      itemBuilder: (context, index){
        return Card(
          child: ListTile(
            title: Text('Time: ' + listData[index].currentTime),
            subtitle: Text('Temperature: ' + listData[index].temp.toString())
          ),
        );
      },
    );
  }
}