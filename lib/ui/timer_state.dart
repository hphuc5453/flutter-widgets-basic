
import 'package:demo_random_number/blocs/temp_bloc.dart';
import 'package:demo_random_number/models/elapsed_time.dart';
import 'package:flutter/material.dart';

class TimerWidget extends StatefulWidget {
  final TempBloc tempBloc;

  TimerWidget(this.tempBloc);

  @override
  State<StatefulWidget> createState() => _TimerState(tempBloc);
}

class _TimerState extends State<TimerWidget> {
  _TimerState(this.tempBloc);

  final TempBloc tempBloc;

  int minutes = 0;
  int seconds = 0;

  void onTick(ElapsedTime elapsedTime) {
    if (elapsedTime.minutes != minutes || elapsedTime.seconds != seconds) {
      setState(() {
        minutes = elapsedTime.minutes;
        seconds = elapsedTime.seconds;
      });
    }

    if(seconds == 10){
      setState(() {
        minutes = 0;
        seconds = 0;
        tempBloc.stopwatch.reset();
        tempBloc.updateNumber();
      });
    }
  }

  @override
  void initState() {
    tempBloc.timerListeners.add(onTick);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    String minutesStr = (minutes % 60).toString().padLeft(2, '0');
    String secondsStr = (seconds % 60).toString().padLeft(2, '0');
    return Text('$minutesStr:$secondsStr',
        style: const TextStyle(fontSize: 25, color: Colors.redAccent));
  }
}
