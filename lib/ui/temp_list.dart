import 'dart:math';

import 'package:demo_random_number/blocs/temp_bloc.dart';
import 'package:demo_random_number/blocs/temp_state.dart';
import 'package:demo_random_number/ui/list_save.dart';
import 'package:demo_random_number/ui/timer_text_state.dart';
import 'package:flutter/material.dart';

class TempListWidget extends StatefulWidget {
  const TempListWidget({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _TempListState();
}

class _TempListState extends State<TempListWidget> {
  final TempBloc tempBloc = TempBloc();

  @override
  void initState() {
    tempBloc.stopwatch.start();
    super.initState();
  }

  @override
  void dispose() {
    tempBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(
        title: const Text('Random Temperature'),
      ),
      body: Center(
        child: StreamBuilder<TempStateBase>(
            stream: tempBloc.stateController.stream,
            initialData: tempBloc.state,
            builder:
                (BuildContext context, AsyncSnapshot<TempStateBase> snapshot) {
              return Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    padding: const EdgeInsets.only(
                        left: 10, top: 40, right: 10, bottom: 10),
                    alignment: Alignment.center,
                    child: Row(
                      children: [
                        const Expanded(
                            child: Text(
                          'Temperature: ',
                          style: TextStyle(fontSize: 20, color: Colors.green),
                        )),
                        Expanded(
                            child: Text(
                          snapshot.data != null
                              ? snapshot.data!.randomNumber.toString()
                              : Random().nextInt(100).toString(),
                          style: const TextStyle(
                              fontSize: 25, color: Colors.redAccent),
                        ))
                      ],
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.all(10),
                    alignment: Alignment.center,
                    child: Row(
                      children: [
                        const Expanded(
                          child: Text('Time: ',
                              style:
                                  TextStyle(fontSize: 20, color: Colors.green)),
                        ),
                        Expanded(child: TimerTextWidget(tempBloc))
                      ],
                    ),
                  ),
                  Expanded(
                    child: Container(
                        padding: const EdgeInsets.all(10),
                        alignment: Alignment.center,
                        child: ListSaveWidget(snapshot.data != null
                            ? snapshot.data!.listData
                            : List.empty())),
                  )
                ],
              );
            }),
      ),
    ));
  }
}
