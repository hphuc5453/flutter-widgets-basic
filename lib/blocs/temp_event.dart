import 'package:demo_random_number/blocs/temp_bloc.dart';
import 'package:demo_random_number/blocs/temp_state.dart';
import 'package:flutter/cupertino.dart';

@immutable
abstract class TempEventsBase{
  Stream<TempStateBase> updateNumber(TempBloc tempBloc);
}

class UpdateTemp extends TempEventsBase{
  @override
  Stream<TempStateBase> updateNumber(TempBloc tempBloc) {
    throw UnimplementedError();
  }
  
}

class TempEvents extends TempEventsBase{
  @override
  Stream<TempStateBase> updateNumber(TempBloc tempBloc) {
    throw UnimplementedError();
  }
}