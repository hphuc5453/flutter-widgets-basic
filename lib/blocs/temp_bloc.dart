import 'dart:async';
import 'dart:math';
import 'package:demo_random_number/blocs/temp_event.dart';
import 'package:demo_random_number/blocs/temp_state.dart';
import 'package:demo_random_number/models/elapsed_time.dart';
import 'package:demo_random_number/models/temperature.dart';
import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';

class TempBloc {
  final List<ValueChanged<ElapsedTime>> timerListeners = <
      ValueChanged<ElapsedTime>>[];
  final Stopwatch stopwatch = Stopwatch();
  final int timerMillisecondsRefreshRate = 100;

  var state = TempStateBase(Random().nextInt(100), <Temperature>[]);

  final eventController = StreamController<TempEventsBase>();
  final stateController = StreamController<TempStateBase>();
  final listStateController = StreamController<TempStateBase>();

  TempBloc() {
    eventController.stream.listen((event) {
      updateNumber();
    });
  }

  void updateNumber() {
    final Temperature temperature = Temperature(state.randomNumber,
        DateFormat('HH:mm:ss').format(DateTime.now()));
    state.listData.add(temperature);
    state.randomNumber = Random().nextInt(100);
    stateController.sink.add(state);
  }

  void dispose() {
    eventController.close();
    stateController.close();
    state.listData.clear();
  }
}