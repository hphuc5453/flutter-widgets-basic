class ElapsedTime{
  final int seconds;
  final int minutes;

  ElapsedTime(this.minutes, this.seconds);
}